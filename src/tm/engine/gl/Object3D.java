package tm.engine.gl;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_SHORT;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glDrawElements;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import tm.engine.gl.shader.Shader;
import tm.engine.gl.shader.ShaderWithSun;
import tm.engine.gl.shader.ShaderWithTexture;
import android.content.Context;

public class Object3D implements Cloneable {

	private FloatBuffer mVertexBuffer;
	private FloatBuffer mNormalBuffer;
	private ShortBuffer mGLIndex;

	// texture
	private int mTextureId;

	private Shader mShader;
	private Camera mCamera;
	private Light mSun;

	// transform
	private Transform mTransform = new Transform();
	private final float[] mProjectionMatrix;

	private float[] mVertexArray;
	private float[] mColorArray;
	private FloatBuffer mTexCoordinate;

	public Object3D(float[] projectionMatrix) {
		mProjectionMatrix = projectionMatrix;
	}

	public void draw(double time, float[] mvpMatrix) {

		mShader.useProgram(mvpMatrix, mTransform.getTransfotmMatrix());

		if (mGLIndex == null) {
			glDrawArrays(GL_TRIANGLES, 0, mVertexArray.length / 3);
		} else {
			mGLIndex.position(0);
			glDrawElements(GL_TRIANGLES, mGLIndex.capacity(), GL_UNSIGNED_SHORT, mGLIndex);
		}
	}

	public void setVertex(float[] vertexArray, short[] index) {
		this.mVertexArray = vertexArray;

		ByteBuffer bvertex = ByteBuffer.allocateDirect(mVertexArray.length * 4);
		bvertex.order(ByteOrder.nativeOrder());
		mVertexBuffer = bvertex.asFloatBuffer();
		mVertexBuffer.position(0);
		mVertexBuffer.put(mVertexArray);
		mVertexBuffer.position(0);
		if (index != null) {
			ByteBuffer bindex = ByteBuffer.allocateDirect(index.length * 2);
			bindex.order(ByteOrder.nativeOrder());
			mGLIndex = bindex.asShortBuffer();
			mGLIndex.put(index).position(0);
		}

	}

	public void setNormal(float[] normalArray) {
		ByteBuffer bnormal = ByteBuffer.allocateDirect(normalArray.length * 4);
		bnormal.order(ByteOrder.nativeOrder());
		mNormalBuffer = bnormal.asFloatBuffer();
		mNormalBuffer.position(0);
		mNormalBuffer.put(normalArray);
		mNormalBuffer.position(0);
	}

	public void setColor(float[] colorArray) {
		this.mColorArray = colorArray;
	}

	public void setCamera(Camera camera) {
		this.mCamera = camera;
	}

	public void setSun(Light sun) {
		this.mSun = sun;
	}

	public void setTexure(int textureId, float[] texCoordinate) {
		mTextureId = textureId;
		ByteBuffer bTextCoord = ByteBuffer.allocateDirect(texCoordinate.length * 4);
		bTextCoord.order(ByteOrder.nativeOrder());
		mTexCoordinate = bTextCoord.asFloatBuffer();
		mTexCoordinate.position(0);
		mTexCoordinate.put(texCoordinate);
		mTexCoordinate.position(0);
	}

	public void init(Context c) {
		// create shader
		Shader shader = null;
		if (mTextureId != 0) {
			// with texture
			if (mCamera != null && mSun != null) {
				// with sun
				// TODO: доделать солнце
				shader = new ShaderWithTexture(c);
				((ShaderWithTexture) shader).linkTexture(mTextureId, mTexCoordinate); // for
																						// test
			} else {
				// without sun
				shader = new ShaderWithTexture(c);
				((ShaderWithTexture) shader).linkTexture(mTextureId, mTexCoordinate);
			}
		} else {
			// without texture

			if (mCamera != null && mSun != null) {
				// with sun
				shader = new ShaderWithSun(c);
				((ShaderWithSun) shader).linkCamera(mCamera);
				((ShaderWithSun) shader).linkLightSource(mSun);
				((ShaderWithSun) shader).linkNormalBuffer(mNormalBuffer);
			} else {
				// with out sun
				shader = new Shader(c);
			}
		}

		shader.linkVertexBuffer(mVertexBuffer);
		shader.linkColorBuffer(mColorArray);

		mShader = shader;
	}

	// transfrom methods
	public void setTransform(Transform transform) {
		mTransform = transform;
	}

	@Override
	public Object3D clone() throws CloneNotSupportedException {
		Object3D clone = new Object3D(mProjectionMatrix);
		clone.mCamera = mCamera;
		clone.mColorArray = mColorArray;
		clone.mGLIndex = mGLIndex;
		clone.mNormalBuffer = mNormalBuffer;
		clone.mSun = mSun;
		clone.mTexCoordinate = mTexCoordinate;
		clone.mTextureId = mTextureId;
		clone.mTransform = mTransform;
		clone.mVertexArray = mVertexArray;
		clone.mVertexBuffer = mVertexBuffer;
		clone.mShader = mShader.clone();
		return clone;
	}
}
