package tm.engine.gl;

import android.opengl.Matrix;

public class Camera {

	private float mX, mY, mZ;
	private float mLookAtX, mLookAtY, mLookAtZ;
	private float mUpX = 0f, mUpY = 1f, mUpZ = 0f;
	private Boolean mNeedUpdate = true;

	private float[] mViewMatrix = new float[16];

	public void init() {
		Matrix.setLookAtM(mViewMatrix, 0, mX, mY, mZ, mLookAtX, mLookAtY, mLookAtZ, mUpX, mUpY, mUpZ);
	}

	public void setPosition(float x, float y, float z) {
		mX = x;
		mY = y;
		mZ = z;
		synchronized (mNeedUpdate) {
			mNeedUpdate = true;
		}
	}

	public void setLookAt(float x, float y, float z) {
		mLookAtX = x;
		mLookAtY = y;
		mLookAtZ = z;
		synchronized (mNeedUpdate) {
			mNeedUpdate = true;
		}
	}

	public void setUp(float upX, float upY, float upZ) {
		mUpX = upX;
		mUpY = upY;
		mUpZ = upZ;
		synchronized (mNeedUpdate) {
			mNeedUpdate = true;
		}
	}

	public float[] getViewMatrix() {
		synchronized (mNeedUpdate) {
			if (mNeedUpdate) {
				init();
				mNeedUpdate = false;
			}
		}
		return mViewMatrix;
	}

	public float getX() {
		return mX;
	}

	public float getY() {
		return mY;
	}

	public float getZ() {
		return mZ;
	}

	public float getLookAtX() {
		return mLookAtX;
	}

	public float getLookAtY() {
		return mLookAtY;
	}

	public float getLookAtZ() {
		return mLookAtZ;
	}
}
