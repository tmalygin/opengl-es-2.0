package tm.engine.gl.shader;

import java.nio.FloatBuffer;

import tm.engine.gl.Camera;
import tm.engine.gl.Light;
import tm.engine.gl.R;
import tm.engine.utils.RawResourceReader;
import android.content.Context;
import android.opengl.GLES20;

public class ShaderWithSun extends Shader {
	private int a_normal_Handle;
	private FloatBuffer normalBuffer;
	private int u_camera_Handle;
	private Camera camera;
	private Light light;
	private int u_lightPosition_Handle;

	public ShaderWithSun(Context c) {
		super(c);
	}

	ShaderWithSun() {
	}

	protected String getVertexShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.vertex_shader_with_sun);
	}

	protected String getFragmentShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.fragment_shader_with_sun);
	}

	public void linkNormalBuffer(FloatBuffer normalBuffer) {
		this.normalBuffer = normalBuffer;
		GLES20.glUseProgram(mProgramHandle);
		a_normal_Handle = GLES20.glGetAttribLocation(mProgramHandle, "a_Normal");

	}

	public void linkCamera(Camera camera) {
		this.camera = camera;
		GLES20.glUseProgram(mProgramHandle);
		u_camera_Handle = GLES20.glGetUniformLocation(mProgramHandle, "u_camera");
	}

	public void linkLightSource(Light light) {
		this.light = light;
		GLES20.glUseProgram(mProgramHandle);
		u_lightPosition_Handle = GLES20.glGetUniformLocation(mProgramHandle, "u_lightPosition");
	}

	public void useProgram(float[] mvpMatrix, float[] transform) {
		super.useProgram(mvpMatrix, transform);

		GLES20.glUniform3f(u_camera_Handle, camera.getX(), camera.getY(), camera.getZ());

		GLES20.glEnableVertexAttribArray(a_normal_Handle);
		GLES20.glVertexAttribPointer(a_normal_Handle, 3, GLES20.GL_FLOAT, false, 0, normalBuffer);

		GLES20.glUniform3f(u_lightPosition_Handle, light.getX(), light.getY(), light.getZ());
	}

	@Override
	public ShaderWithSun clone() throws CloneNotSupportedException {
		ShaderWithSun clone = new ShaderWithSun();
		clone.mProgramHandle = mProgramHandle;
		return clone;
	}

}
