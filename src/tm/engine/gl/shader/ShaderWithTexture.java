package tm.engine.gl.shader;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glVertexAttribPointer;

import java.nio.FloatBuffer;

import android.content.Context;
import android.opengl.GLES20;
import tm.engine.gl.R;
import tm.engine.utils.RawResourceReader;

public class ShaderWithTexture extends Shader {
	private static final String TAG = ShaderWithTexture.class.getSimpleName();
	private FloatBuffer mTextureBuffer;
	private int mTextureId;
	private int mTexCoordinateHandler;

	public ShaderWithTexture(Context c) {
		super(c);
	}

	protected String getVertexShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.vertex_shader_with_texture);
	}

	protected String getFragmentShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.fragment_shader_wit_texture);
	}

	public void linkTexture(int textureId, FloatBuffer textureBuffer) {
		this.mTextureId = textureId;
		this.mTextureBuffer = textureBuffer;

		GLES20.glUseProgram(mProgramHandle);

		mTexCoordinateHandler = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");
	}

	public void useProgram(float[] mMVPMatrix, float[] transform) {
		super.useProgram(mMVPMatrix, transform);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mTextureId);

		GLES20.glEnableVertexAttribArray(mTexCoordinateHandler);
		GLES20.glVertexAttribPointer(mTexCoordinateHandler, 2, GLES20.GL_FLOAT, false, 0, mTextureBuffer);

	}
}
