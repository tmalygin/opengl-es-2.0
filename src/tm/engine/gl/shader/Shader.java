package tm.engine.gl.shader;

import java.nio.FloatBuffer;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;
import tm.engine.gl.R;
import tm.engine.utils.L;
import tm.engine.utils.RawResourceReader;

public class Shader implements Cloneable {
	private static final String TAG = Shader.class.getSimpleName();
	protected int mProgramHandle;
	private FloatBuffer mVertexBuffer;

	// Handler
	private int mVertexHandle;
	private int mColorHandler;
	private float[] mColor;

	private int mMVPMatrixHandler;
	private int mTransformHandler;

	public Shader(Context c) {
		createProgram(getVertexShader(c), getFragmentShader(c));
		GLES20.glUseProgram(mProgramHandle);
		mMVPMatrixHandler = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
		mTransformHandler = GLES20.glGetUniformLocation(mProgramHandle, "u_transform");
	}

	Shader() {
	}

	protected String getVertexShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.vertex_shader);
	}

	protected String getFragmentShader(Context c) {
		return RawResourceReader.readTextFileFromRawResource(c, R.raw.fragment_shader);
	}

	protected int loadShader(final int shaderType, final String shaderSource) {
		int shaderHandle = GLES20.glCreateShader(shaderType);

		if (shaderHandle != 0) {
			GLES20.glShaderSource(shaderHandle, shaderSource);
			GLES20.glCompileShader(shaderHandle);
			// get compiler status
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			// If the compilation failed, delete the shader.
			if (compileStatus[0] == 0) {
				L.w(TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shaderHandle));
				GLES20.glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}

		if (shaderHandle == 0) {
			throw new RuntimeException("Error creating shader.");
		}

		return shaderHandle;
	}

	private void createProgram(String vertexShaderCode, String fragmentShaderCode) {
		int vertexShaderHandle = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
		int fragmentShaderHandle = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
		int programHandle = GLES20.glCreateProgram();

		if (programHandle != 0) {
			GLES20.glAttachShader(programHandle, vertexShaderHandle);
			GLES20.glAttachShader(programHandle, fragmentShaderHandle);
			GLES20.glLinkProgram(programHandle);

			// Get the link status.
			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

			// If the link failed, delete the program.
			if (linkStatus[0] == 0) {
				Log.e(TAG, "Error compiling program: " + GLES20.glGetProgramInfoLog(programHandle));
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}

		if (programHandle == 0) {
			throw new RuntimeException("Error creating program.");
		}
		this.mProgramHandle = programHandle;
	}

	// link methods
	public void linkVertexBuffer(FloatBuffer vertexBuffer) {
		this.mVertexBuffer = vertexBuffer;
		GLES20.glUseProgram(mProgramHandle);
		mVertexHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");
	}

	public void linkColorBuffer(float[] color) {
		this.mColor = color;
		GLES20.glUseProgram(mProgramHandle);
		mColorHandler = GLES20.glGetUniformLocation(mProgramHandle, "u_color");
	}

	public void useProgram(float[] modelViewProjectionMatrix, float[] transform) {
		GLES20.glUseProgram(mProgramHandle);

		GLES20.glEnableVertexAttribArray(mVertexHandle);
		GLES20.glVertexAttribPointer(mVertexHandle, 3, GLES20.GL_FLOAT, false, 0, mVertexBuffer);

		GLES20.glUniform4fv(mColorHandler, 1, this.mColor, 0);
		GLES20.glUniformMatrix4fv(mTransformHandler, 1, false, transform, 0);
		GLES20.glUniformMatrix4fv(mMVPMatrixHandler, 1, false, modelViewProjectionMatrix, 0);
	}

	@Override
	public Shader clone() throws CloneNotSupportedException {
		Shader clone = new Shader();
		clone.mProgramHandle = mProgramHandle;
		clone.mMVPMatrixHandler = mMVPMatrixHandler;
		clone.linkColorBuffer(mColor);
		clone.linkVertexBuffer(mVertexBuffer);
		return clone;
	}
}
