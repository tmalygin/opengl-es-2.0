package tm.engine.gl;

import android.opengl.Matrix;

public class Transform {

	private float mX, mY, mZ;
	private float mAngle, mRotateX, mRotateY, mRotateZ;
	private float mZoomX, mZoomY, mZoomZ;
	private Boolean mNeedUpdate = Boolean.TRUE;
	private final float[] mTransfromMatrix = new float[16];

	public void setTranslate(float x, float y, float z) {
		mX = x;
		mY = y;
		mZ = z;
		synchronized (mNeedUpdate) {
			mNeedUpdate = Boolean.TRUE;
		}
	}

	public float getX() {
		return mX;
	}

	public float getY() {
		return mY;
	}

	public float getZ() {
		return mZ;
	}

	public void setRotate(float angle, float x, float y, float z) {
		mAngle = angle;
		mRotateX = x;
		mRotateY = y;
		mRotateZ = z;
		synchronized (mNeedUpdate) {
			mNeedUpdate = Boolean.TRUE;
		}
	}

	public float getAngle() {
		return mAngle;
	}

	public float getRotateX() {
		return mRotateX;
	}

	public float getRotateY() {
		return mRotateY;
	}

	public float getRotateZ() {
		return mRotateZ;
	}

	public void setZoom(float zoomX, float zoomY, float zoomZ) {
		mZoomX = zoomX;
		mZoomY = zoomY;
		mZoomZ = zoomZ;
		synchronized (mNeedUpdate) {
			mNeedUpdate = Boolean.TRUE;
		}
	}

	public float getZoomX() {
		return mZoomX;
	}

	public float getZoomY() {
		return mZoomY;
	}

	public float getZoomZ() {
		return mZoomZ;
	}

	public void update() {
		Matrix.setIdentityM(mTransfromMatrix, 0);

		Matrix.translateM(mTransfromMatrix, 0, mX, mY, mZ);
		if (mRotateX != 0)
			Matrix.rotateM(mTransfromMatrix, 0, mAngle, mRotateX, 0f, 0f);

		if (mRotateY != 0)
			Matrix.rotateM(mTransfromMatrix, 0, mAngle, 0f, mRotateY, 0f);

		if (mRotateZ != 0)
			Matrix.rotateM(mTransfromMatrix, 0, mAngle, 0f, 0f, mRotateZ);

		Matrix.scaleM(mTransfromMatrix, 0, mZoomX, mZoomY, mZoomZ);
		mNeedUpdate = Boolean.FALSE;
	}

	public float[] getTransfotmMatrix() {
		synchronized (mNeedUpdate) {
			if (mNeedUpdate) {
				update();
			}
		}
		return mTransfromMatrix;
	}

	public void multiplyVertexWithTransform(float[] vertex) {
		float[] transform = getTransfotmMatrix();
		float[] position = new float[4];
		float[] result = new float[4];

		for (int i = 0; i < vertex.length; i += 3) {
			position[0] = vertex[i];
			position[1] = vertex[i + 1];
			position[2] = vertex[i + 2];
			position[3] = 1;

			Matrix.multiplyMV(result, 0, transform, 0, position, 0);
			vertex[i] = result[0];
			vertex[i + 1] = result[1];
			vertex[i + 2] = result[2];
		}
	}

	public static void sub(Transform result, Transform transformA, Transform transformB) {
		// float[] localTransform = transformA.getTransfotmMatrix();
		// float[] transfotmMatrix = transformB.getTransfotmMatrix();
		//
		// int count = localTransform.length;
		// for (int i = 0; i < count; i++) {
		// result.mTransfromMatrix[i] = localTransform[i] + transfotmMatrix[i];
		// result.mNeedUpdate = false;
		// }

		result.setRotate(transformA.getAngle() + transformB.getAngle(),
				transformA.getRotateX() + transformB.getRotateX(), transformA.getRotateY() + transformB.getRotateY(),
				transformA.getRotateZ() + transformB.getRotateZ());
		result.setTranslate(transformA.getX() + transformB.getX(), transformA.getY() + transformB.getY(),
				transformA.getZ() + transformB.getZ());
		result.setZoom(transformA.getZoomX() + transformB.getZoomX(), transformA.getZoomY() + transformB.getZoomY(),
				transformA.getZoomZ() + transformB.getZoomZ());
	}
}
