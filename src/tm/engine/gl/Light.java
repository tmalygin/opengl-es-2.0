package tm.engine.gl;

public class Light {

	private float mX, mY, mZ;

	public void setPosition(float x, float y, float z) {
		mX = x;
		mY = y;
		mZ = z;
	}

	public float getX() {
		return mX;
	}

	public float getY() {
		return mY;
	}

	public float getZ() {
		return mZ;
	}
}
