package tm.engine.utils;

import android.util.Log;

public class L {

	private static final String TAG = "Helmet";
	public static final boolean DEBUG = true;

	private L() {
	}

	public static final void v(String tag, String msg) {
		if (DEBUG)
			Log.v(TAG, "[ " + tag + " ]: " + msg);
	}

	public static final void v(String tag, String format, Object... params) {
		if (DEBUG)
			v(TAG, String.format(format, params));
	}

	public static final void w(String tag, String wrn) {
		if (DEBUG)
			Log.w(TAG, wrn);
	}

	public static final void e(String tag, Throwable thr) {
		Log.e(TAG, tag, thr);
	}
}
