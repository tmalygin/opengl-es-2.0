package tm.engine.utils;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

public class TextureHelper {

	private static final String TAG = TextureHelper.class.getSimpleName();

	public static int loadTextureFromAssets(final Context c, final String path) throws IOException {
		InputStream open = null;
		int textureId = 0;
		try {
			open = c.getAssets().open(path);
			final Bitmap bmp = BitmapFactory.decodeStream(open);
			textureId = createTexture(bmp);
		} finally {
			if (open != null) {
				try {
					open.close();
				} catch (Exception e) {
				}
			}
		}
		return textureId;
	}

	/**
	 * 
	 * @param context
	 * @param resourceId
	 *            - Resources from R class
	 * @return
	 */
	public static int loadTextureFromResources(final Context context, final int resourceId) {

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false; // No pre-scaling

		// Read in the resource
		final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

		return createTexture(bitmap);
	}

	public static int createTexture(Bitmap bitmap) {
		final int[] textureHandle = new int[1];

		GLES20.glGenTextures(1, textureHandle, 0);

		if (textureHandle[0] != 0) {
			// Bind to the texture in OpenGL
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

			// Set filtering
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);

			// Load the bitmap into the bound texture.
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

			// Recycle the bitmap, since its data has been loaded into OpenGL.
			bitmap.recycle();
		}

		if (textureHandle[0] == 0) {
			L.w(TAG, "Error loading texture ");
		}

		return textureHandle[0];
	}
}
